param([Boolean]$in=$true)

if($in){
    Get-Clipboard | % {$_.replace("<","&lt;")} | % {$_.replace(">","&gt;")} | Set-Clipboard
}
else{
    Get-Clipboard | % {$_.replace("&lt;","<")} | % {$_.replace("&gt;",">")} | Set-Clipboard
}