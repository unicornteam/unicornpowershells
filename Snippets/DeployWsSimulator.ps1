param([String]$sourcePath="C:\tfs\DEV\ExtWS\ExternalWS_PPVNEM", [String]$targetPath="Z:\inetpub\wwwroot\POJ_WS_SIMULATORS_DEV2")

# Copy-Item $targetPath "$targetPath\BACKUP" -Recurse  

# Copy-Item $sourcePath $targetPath -Recurse

$excludes = ".vs","CVS"
Get-ChildItem $sourcePath -Directory | 
    Where-Object{$_.Name -notin $excludes} | 
    Copy-Item -Destination $targetPath -Recurse -Force