param([String]$replace="", [String]$with="")

Get-Clipboard | % {$_.replace($replace, $with)} | Set-Clipboard