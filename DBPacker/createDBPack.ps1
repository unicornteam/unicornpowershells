﻿<#

.SYNOPSIS
This is a simple Powershell script to pack given db scripts.

.DESCRIPTION
The script will pack selected sql scripts into one, create readme and email files from given templates, fills them with parameters and then archives the output sql script and readme.
In order to successfully run the script, you must provide \template.sql \readmeTemplate.txt and \emailTemplate.txt in script root directory.
These templates must have contain following variables:
\template.sql
{0} - version
{1} - content

\readmeTemplate.txt
{0} - version
{1} - author
{2} - description
{3} - date
{4} - schema

\emailTemplate.txt
{0} - version
{1} - description
{2} - runtime estimation
{3} - schema
{4} - parametry

Result of this script is "$version".zip(containing "$version".sql and readme.txt) and email.txt
Please dont forget to pass parameters or edit them in script content.

.EXAMPLE
./createDBPack.ps1
Note: Params where edited inside script.
.EXAMPLE
./createDBPack.ps1 -version "POJ.103.1255" -author "Tomas Bouda" -description "Some description" -runTimeEst "5 min" -schema "POJWEB" -scriptParams "Zadat číslo okresu: 557"

.NOTES
Requires PS 5.1 to run (Compress-Archive)

.LINK
https://bitbucket.org/unicornteam/unicornpowershells/
#>

# *** Edit params here :] ***
param([String]$version="POJ.103.1255", [String]$author="Tomáš Bouda", [String]$description="", [String]$runTimeEst="5 min", [String]$schema="POJWEB", [String]$scriptParams="Zadat číslo okresu: 557", [Boolean]$cleanDir=$true)

function Get-FileNames($initialDirectory)
{   
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") |
    Out-Null

    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.Multiselect = 1
    $OpenFileDialog.filter = "All files (*.*)| *.*"
    $OpenFileDialog.ShowDialog() | Out-Null

    return $OpenFileDialog.FileNames
}

$dateToday = Get-Date -UFormat "%d.%m.%Y"

# Load template files
$sqlTemplate = [IO.File]::ReadAllText("$PWD\template.sql", [Text.Encoding]::UTF8)
$readmeTemplate = [IO.File]::ReadAllText("$PWD\readmeTemplate.txt", [Text.Encoding]::GetEncoding("windows-1250"))
$emailTemplate =  [IO.File]::ReadAllText("$PWD\emailTemplate.txt", [Text.Encoding]::GetEncoding("windows-1250"))

# Get input sql files from user
$scriptFiles = Get-FileNames $PWD.Path | Resolve-Path

# Crete script content from sql files
$content = "";
foreach ($file in $scriptFiles)
{
    $fileName = Split-Path $file -leaf
    $fileName = $fileName.Replace(".sql", ".lst")

    $content += ([Environment]::NewLine) + "SPOOL &SPOOLNAME." + $fileName + ([Environment]::NewLine)
    $content += (Get-Content -Path $file) -join "`n"
    $content += ([Environment]::NewLine) + "/" + ([Environment]::NewLine)
    $content += "SPOOL Off" + ([Environment]::NewLine)
}

# Create temp output directory
$newDir = New-Item -ItemType Directory -Force -Path "$PWD\$version"

# Fill in all variables into output sql file
# {0} - version
# {1} - content
$outSql = $sqlTemplate -f $version, $content
$outSql | Out-File "$newDir\$version.sql" -encoding ascii

# Fill in all variables into reame.txt
# {0} - version
# {1} - author
# {2} - description
# {3} - date
# {4} - schema
$readme = $readmeTemplate -f $version, $author, $description, $dateToday, $schema
$readme | Out-File "$newDir\readme.txt" -encoding UTF8

# Fill in all variables into email.txt
# {0} - version
# {1} - description
# {2} - runtime estimation
# {3} - schema
# {4} - parametry
$email = $emailTemplate -f $version, $description, $runTimeEst, $schema, $scriptParams
$email | Out-File "$PWD\email.txt" -encoding UTF8

# If Compress-Archive cmdlet is not available, please comment out following lines
# Create zip archive from sql and readme file
Compress-Archive -Path $newDir -DestinationPath "$PWD\$version.zip" -Force

if($cleanDir)
{
    # Remove temporary directory
    Remove-Item $newDir -Force -Recurse
}

Write-Host "Successfully generated ;] `n$PWD\$version.zip `n$PWD\email.txt" -ForegroundColor Green