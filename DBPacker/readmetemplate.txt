﻿{0}
-------------------------------------------------------------------------------
Autor: {1}
Účel:  {2}
Datum: {3}
Verze 1.0 
-------------------------------------------------------------------------------

Pro spravne provedeni je potreba nastavit 
parametr NLS_LANG na hodnotu "CZECH_CZECH REPUBLIC.EE8MSWIN1250"

Postup pro spusteni:
1. Pod uzivatelem {4} spustit skript {0}.sql
2. Vygenerovane logy zaslat zadavateli.