--{0}

set echo off
set long 640000
set linesize 4000
set serveroutput on
set heading on
set verify off
set trimout on
set trimspool on
set pages 9999
-- nastaveni zobrazeni NULL hodnot 
set null "*"
-- nastaveni oddelovace sloupcu
set colsep ";";

DEFINE PKG_NAME= {0}
UNDEFINE SPOOLNAME;

VAR SPOOLNAME VARCHAR2(80 CHAR) ;

EXEC :SPOOLNAME:= '&&PKG_NAME._' || SYS_CONTEXT('USERENV','DB_NAME') || '_' || SYS_CONTEXT('USERENV','INSTANCE') || '_' || SYS_CONTEXT('USERENV','CURRENT_USER') ;

COLUMN SPOOLNAME NEW_VALUE SPOOLNAME

SELECT :SPOOLNAME AS SPOOLNAME FROM DUAL; 

SPOOL &SPOOLNAME..lst

select  to_CHAR(systimestamp, 'DD.MM.YYYY HH24:MI:SS,FF') zacatek_skriptu from dual;
show user
select 'DBNAME is "' ||sys_context('userenv','db_name')||'"' dbname from dual;
prompt Vytvarim lst soubor...
spool off

{1}

SPOOL &SPOOLNAME..lst append
select 'Konec skriptu: ' || to_CHAR(systimestamp, 'DD.MM.YYYY HH24:MI:SS,FF') KONEC from dual;

spool off